$(document).ready(function() {
    var arrowScrollBtn = $('.arrow-scroll-btn');
    var headerSidebar = $('.site-header .site-header__sidebar-menu');
    var headerStickyBtnToggle = $('.site-header .site-header__sticky-btn-toggle');
    var sideBarDropdown = $('.site-header__sidebar-dropdown');
    var sideBarCloseBtn = $('.site-header__hidden-area-close');

    $(window).on('scroll', function(event) {
        if ($(this).scrollTop() > 35) {
            arrowScrollBtn.fadeIn(1000);
        } else {
            arrowScrollBtn.fadeOut(1000);
        }
    });
    arrowScrollBtn.on('click', function(event) {
        event.preventDefault();
        $('html, body').animate({
            scrollTop: 0
        }, 1000);
    });
    headerStickyBtnToggle.on('click', function(event) {
        event.preventDefault();
        $(this).parents('.site-header__sidebar-menu').addClass('active');
        headerSidebar.find('.site-header__hidden-area').addClass('active');
        arrowScrollBtn.hide();
    });
    sideBarCloseBtn.on('click', function(event) {
        event.preventDefault();
        $(this).parents('.site-header__sidebar-menu').removeClass('active');
        headerSidebar.find('.site-header__hidden-area').removeClass('active');
    });
    sideBarDropdown.styler();
});
